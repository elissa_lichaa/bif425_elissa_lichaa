#!/bin/bash

## ELISSA LICHAA EL KHOURY 
## FUNCTIONAL GENOMICS
## FINAL PROJECT

// copied the files to my directory /mnt/elissa
cp 392_1.fastq.gz /mnt/elissa
cp 392_2.fastq.gz /mnt/elissa



#preformed fastqc on the files , before trimming
#fastqc version : FastQC v0.11.9 


time ./FastQC/fastqc 392_1.fastq.gz

#time:
#real	7m9.723s
#user	7m5.278s
#sys	0m7.869s

time ./FastQC/fastqc 392_2.fastq.gz

#time 
#real	7m7.237s
#user	7m3.304s
#sys	0m8.033s



# According to the html file, illumina version 1.9 was used.  
#Tiles with bad quality if any: According to the per tile quality score, the tile qualities are very good, since the graph is completely blue, this result is expected since the per base sequence quality is also very high. There are no tiles with bad quality.
# run trimmomatic command in order to remove adapters and trim our data. 
# we put as a parameter our 2 files, 392_1 and 392_2 . the trimming tool will output 4 files:
# 392_trimmed_1 contains all reads that met requirements of our trimming arguments and included paired version and had complementary 392_2 sequence. 
# the unpaired files will include all 392_1 reads that met requirement and had complementary 392_2 that did not meet requirements. 
# we trim our files according to the following parameters : 
# After running the command using different adapter files in order to check which one removes them the most. so we preform the trimming based on the illumina clip 
# LEADING 3, means minimum to keep leading 5'. 
# TRAILING : 5, minimum to keep leading 3'. 
# SLIDINGWINDOW: 4:15 , means takes 4 positions at a time checks for the average quality score for these 4 positions if it is lower then 15 it trims. 
#MINLEN:36 , minimum length=36


time java -jar /usr/src/Trimmomatic-0.36/trimmomatic-0.36.jar PE \
-threads 8 \-trimlog ./392.log \
./392_1.fastq.gz \
./392_2.fastq.gz \
./392_trimmed_1_paired.fastq.gz \
./392_trimmed_1_unpaired.fastq.gz \
./392_trimmed_2_paired.fastq.gz \
./392_trimmed_2_unpaired.fastq.gz \
ILLUMINACLIP:/usr/src/Trimmomatic-0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
LEADING:3 \
TRAILING:3 \
SLIDINGWINDOW:4:15 \
MINLEN:36

#trimmomatic version used :Trimmomatic-0.36
#time: 
# real	29m48.184s
# user	84m19.452s
# sys	49m16.275s

#Tiles with bad quality if any: According to the per tile quality score, the tile qualities are very good, since the graph is completely blue, this result is expected since the per base sequence quality is also very high. 


# run fastqc again on the trimmed paired files, to check if adapters hav ebeen succesfully removed. 

time ./FastQC/fastqc 392_trimmed_1_paired.fastq.gz

#time:
#real	6m32.512s
#user	6m31.343s
#sys	0m5.171s

time ./FastQC/fastqc 392_trimmed_2_paired.fastq.gz

real	6m36.627s
user	6m35.909s
sys	0m5.013s

#compare fastqc before and after trimmomatic : Validation of trimming plots
#READ ONE
#the total number of sequences decreased after trimming, before trimming the sequence length was 30895087 after trimming 
#Per base sequence quality and per tile sequence quality :the quailty of the reads before and after trimming is high , the tiles also have high quality 
#Adapters have been succesfully removed as we can see in the adapter content plot. Before trimming there was ! 
# READ 2 before and after trimming 
# the sequence length varies : before trimming it was 151 after trimming ti became 36-151 
# adapters have been succesfully removed after trimming 


#Length of remaining reads after trimming: range between 36-151 (from html file preformed on data after trimming ) 

# downloaded the reference genome chromosome 4 with all the patches after creating a new directory (chrm4) and downloading the data into the new directory
mkdir chrm4
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_GL000257v2_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_GL000008v2_random.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_GL383527v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_GL383528v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270785v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270786v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270787v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270788v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270789v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270790v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270896v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270925v1_alt.fa.gz


#concatenate the files in to file , no neccesary to tar because already unzipped
zcat ./*.gz > chrm4.fa
cp chrm4.fa /mnt/elissa
cd ..
rm -r chrm4


#indexing reference genome using bwa :

time bwa index -p chrm4bwaidx -a bwtsw chrm4.fa

#time:
#real	3m55.782s
#user	3m44.882s
#sys	0m1.236s


# version of bwa: 
Version: 0.7.17-r1188 

#bwa : mapps our reference to trimmed paired reads according to a read group : 

time bwa mem -t 8 -R "@RG\tID:rg1\tSM:sample1\tPL:illumina\tLB:lib1\tPU:HNLHYDSXX:1:GCCGGACA+TGTAAGAG" ./chrm4bwaidx ./392_trimmed_1_paired.fastq.gz ./392_trimmed_2_paired.fastq.gz > ./392.sam

#time :
#real	40m0.384s
#user	318m52.386s
#sys	2m36.447s


#convert sam to bam 

time samtools fixmate -O bam 392.sam 392.bam

#time 
# real	10m59.235s
#user	10m20.262s
#sys	0m37.517s

#VALIDATE BAM FILE : make sure no errors 
#gatk version: gatk-4.1.9.0
./gatk-4.1.9.0/gatk ValidateSamFile INPUT=392.bam MODE=SUMMARY
## tool retruns 0, no error found 



#sort bam file 
./gatk-4.1.9.0/gatk SortSam INPUT=392.bam OUTPUT=392_sorted.bam SORT_ORDER=coordinate

#time 
#real	7m58.588s
#user	36m40.184s
#sys	3m43.817s


# Marking duplicates
time ./gatk-4.1.9.0/gatk MarkDuplicates \
> INPUT=392_sorted.bam \
> OUTPUT=392_dedup.bam \
> METRICS_FILE=392.metrics

#time
#real	7m48.722s
#user	148m20.796s
#sys	2m43.920s

#create sequence dictionary 

time gatk-4.1.9.0/gatk CreateSequenceDictionary R=chrm4.fa O=chrm4.dict

#time 
#real	0m7.366s
#user	1m36.666s
#sys	0m31.882s

#index file
samtools faidx ./chrm4.fa

#version of samtools used:1.4

# Base recalibrator 
time gatk-4.1.9.0/gatk BaseRecalibrator -I 392_dedup.bam -R chrm4.fa --known-sites /mnt/NGSdata/snpdb151_All_20180418.vcf -O recal_data.table

#time :
#real	9m19.697s
#user	13m52.873s
#sys	28m49.820s


#ApplyBQSR 
time gatk-4.1.9.0/gatk ApplyBQSR -R chrm4.fa -I 392_dedup.bam --bqsr-recal-file recal_data.table -O output_recal.bam 

#time :
#real	21m40.721s
#user	30m16.757s
#sys	4m6.490s

#index dedup bam
samtools index 392_dedup.bam

#Haplotype caller 
time ./gatk-4.1.9.0/gatk --java-options "-Xmx4g" HaplotypeCaller -R chrm4.fa -I 392_dedup.bam -O output.g.vcf.gz -ERC GVCF

#time :
#real	119m55.479s
#user	180m41.862s
#sys	16m16.227s

#generate VCF file 

time ./gatk-4.1.9.0/gatk --java-options "-Xmx4g" GenotypeGVCFs -R chrm4.fa -V output.g.vcf.gz -O output.vcf.gz

#time :
#real	2m38.378s
#user	3m30.461s
#sys	0m9.663s

#generate annotated vcf file
time ./gatk-4.1.9.0/gatk --java-options "-Xmx8g" VariantAnnotator -R ./chrm4.fa -V output.vcf.gz --dbsnp /mnt/NGSdata/snpdb151_All_20180418.vcf --output annotated.vcf.gz

#time :
#real	5m54.092s
#user	16m47.881s
#sys	36m19.622s


#Number of Duplicates

time samtools view -c -f 0x400 392_dedup.bam

#number of duplicates : 1138516

#time 
#real	1m1.845s
#user	1m0.079s
#sys	0m1.724s

## Number of supplementary and secondary read 

time samtools view -c -f 0x900 392_dedup.bam

# number of supplementary and secondary reads : 0
# real	1m10.671s
#user	1m8.898s
#sys	0m1.726s


##Number of reads without a pair complement: 
awk '{print $7}' 392.sam > paired_end_column
awk '/*/{++cnt} END {print "Count = ", cnt}' paired_end_column
#Count =  54448424


##Percentage and Number of reads mapped.
time samtools view -c -F 0x4 392_dedup.bam
 
# number : 7610642

#time: 
#real	1m6.873s
#user	1m5.091s
#sys	0m1.737s

samtools flagstat 392.bam

#12.26% unmapped. 
#62059066 + 0 in total (QC-passed reads + QC-failed reads)
#0 + 0 secondary
#659646 + 0 supplementary
#0 + 0 duplicates
#7610642 + 0 mapped (12.26% : N/A)
#61399420 + 0 paired in sequencing
#30699710 + 0 read1
#30699710 + 0 read2
#0 + 0 properly paired (0.00% : N/A)
#6950996 + 0 with itself and mate mapped
#0 + 0 singletons (0.00% : N/A)
#89082 + 0 with mate mapped to a different chr
#25 + 0 with mate mapped to a different chr (mapQ>=5)




# Average Mapping score/quality for the mapped reads.

grep -n "=" 392.sam > mapped_reads
awk '{ total += $5; count++ } END { print total/count }' mapped_reads

# Average Mapping score/quality for the mapped reads =24.2117

# Use the CIGAR string, to compute the number of reads without any Insertion or Deletion 

awk '{print $6}' 392.sam > cigar_string
grep -v 'D\|I' cigar_string  > reads_withoutid
cat reads_withoutid | wc -l

#n=60922679

#Number of variants
./gatk-4.1.9.0/gatk CountVariants -V output.vcf

#Tool returned:
#418363


#Number of SNP
./gatk-4.1.9.0/gatk SelectVariants \
     -R chrm4.fa \
     -V output.vcf \
     --select-type-to-include SNP \
     -O snp.vcf
     
./gatk-4.1.9.0/gatk CountVariants -V snp.vcf

#Number : 386884

#Number of INDELS

./gatk-4.1.9.0/gatk SelectVariants \
     -R chrm4.fa \
     -V output.vcf \
     --select-type-to-include INDEL \
     -O indel.vcf
     
./gatk-4.1.9.0/gatk CountVariants -V indel.vcf

#Number: 31016
#bcftools stats output.vcf > file.stats

#Number of heterozygotes 
awk '{print $10}' output.vcf > column10
grep -n "0|1:" column10 > heterozygote
grep -n "0/1:" column10 > heterozygotes
grep -n "1/2:" column10 > heterozygotes2
grep -n "0/2:" column10 > heterozygotes3

cat heterozygote | wc -l   #151663
cat heterozygotes | wc -l  #49481
cat heterozygotes2 | wc -l #3589
cat heterozygotes3 | wc -l #0

#number : 204703

# Number of homozygous wildtype
grep -n "0|0:" column10 > homozygous

cat homozygous | wc -l

#time 
#real	0m1.845s
#user	0m0.079s
#sys	0m1.724s

#number :0 

#Number of homygous mutant 

grep -n "1|1:" column10 > homozygous_mutants1
grep -n "1/1:" column10 > homozygous_mutants2
grep -n "2/2:" column10 > homozyous_mutants3

cat homozygous_mutants1 | wc -l  #130256 
cat homozygous_mutants2 | wc -l  #83374
cat homozygous_mutants3 | wc -l  #0


#number :213630







     
     





