#!/bin/bash
// copied the files to my directory /mnt/elissa
cp 392_1.fastq.gz /mnt/elissa
cp 392_2.fastq.gz /mnt/elissa



#did fastqc on the files , before trimming
# fastqc version : FastQC v0.11.9 


time ./FastQC/fastqc 392_1.fastq.gz

#time:
#real	7m9.723s
#user	7m5.278s
#sys	0m7.869s

time ./FastQC/fastqc 392_2.fastq.gz

#time 
#real	7m7.237s
#user	7m3.304s
#sys	0m8.033s



# According to the html file, illumina version 1.9 was used. 

# run trimmomatic command in order to remove adapters and trim our data. 
# we put as a parameter our 2 files, 392_1 and 392_2 . the trimming tool will output 4 files:
# 392_trimmed_1 contains all reads that met requirements of our trimming arguments and included paired version and had complementary 392_2 sequence. 
# the unpaired files will include all 392_1 reads that met requirement and haad complementary 392_2 that did not meet requirements. 
# we trim our files according to the following parameters : 
# After running the command using different adapter files in order to check which one removes them the most. so we preform the trimming based on the illumina clip 
# LEADING 3, means minimum to keep leading 5'. 
# TRAILING : 5, minimum to keep leading 3'. 
# SLIDINGWINDOW: 4:15 , means takes 4 positions at a time checks for the average quality score for these 4 positions if it is lower then 15 it trims. 


time java -jar /usr/src/Trimmomatic-0.36/trimmomatic-0.36.jar PE \
-threads 8 \-trimlog ./392.log \
./392_1.fastq.gz \
./392_2.fastq.gz \
./392_trimmed_1_paired.fastq.gz \
./392_trimmed_1_unpaired.fastq.gz \
./392_trimmed_2_paired.fastq.gz \
./392_trimmed_2_unpaired.fastq.gz \
ILLUMINACLIP:/usr/src/Trimmomatic-0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
LEADING:3 \
TRAILING:3 \
SLIDINGWINDOW:4:15 \
MINLEN:36

#time: 
# real	29m48.184s
# user	84m19.452s
# sys	49m16.275s

#Tiles with bad quality if any: According to the per tile quality score, the tile qualities are very good, since the graph is completely blue, this reuslt is expected since the per base sequence quality is also very high. 


# run fastqc again on the trimmed files, to check if adapters hav ebeen succesfully removed. 

time ./FastQC/fastqc 392_trimmed_1_paired.fastq.gz

#time
#real	6m32.512s
#user	6m31.343s
#sys	0m5.171s

time ./FastQC/fastqc 392_trimmed_2_paired.fastq.gz

real	6m36.627s
user	6m35.909s
sys	0m5.013s

#compare fastqc before and after trimmomatic : Validation of trimming plots
#READ ONE
#the total number of sequences decreased after trimming, before trimming the sequence length was 30895087 after trimming 
#Per base sequence quality and per tile sequence quality :the quailty of the reads before and after trimming is high , the tiles also have high quality 
#Adapters have been succesfully removed as we can see in the adapter content plot. Before trimming there was ! 
# READ 2 before and after trimming 
# the sequence length varies : before trimming it was 151 after trimming ti became 36-151 
# adapters have been succesfully removed after trimming 




# downloaded the reference genome chromosome 4 with all the patches after creating a new directory (chrm4) and downloading the data into the new directory
mkdir chrm4
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_GL000257v2_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_GL000008v2_random.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_GL383527v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_GL383528v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270785v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270786v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270787v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270788v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270789v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270790v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270896v1_alt.fa.gz
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr4_KI270925v1_alt.fa.gz


#concatenate the files in to file , no neccesary to tar because already unzipped
zcat ./*.gz > chrm4.fa
cp chrm4.fa /mnt/elissa
cd ..
rm -r chrm4


#indexing reference genome using bwa :

time bwa index -p chrm4bwaidx -a bwtsw chrm4.fa

#time:
#real	3m55.782s
#user	3m44.882s
#sys	0m1.236s


# version of bwa: 
Version: 0.7.17-r1188 

#bwa 

time bwa mem -t 8 -R "@RG\tID:rg1\tSM:sample1\tPL:illumina\tLB:lib1\tPU:HNLHYDSXX:1:GCCGGACA+TGTAAGAG" ./chrm4bwaidx ./392_trimmed_1_paired.fastq.gz ./392_trimmed_2_paired.fastq.gz > ./392.sam

#time :
#real	40m0.384s
#user	318m52.386s
#sys	2m36.447s


#convert sam to bam 

time samtools fixmate -O bam 392.sam 392.bam

#time 
# real	10m59.235s
#user	10m20.262s
#sys	0m37.517s


#create sequence dictionary 

time gatk-4.1.9.0/gatk CreateSequenceDictionary R=chrm4.fa O=chrm4.dict

#time 
#real	0m7.366s
#user	1m36.666s
#sys	0m31.882s

#index file
samtools faidx ./chrm4.fa

# Base recalibrator 
time gatk-4.1.9.0/gatk BaseRecalibrator -I 392.bam -R chrm4.fa --known-sites /mnt/NGSdata/snpdb151_All_20180418.vcf -O recal_data.table










